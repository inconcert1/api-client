import es6Promise from 'es6-promise';
import RESTClient from 'rest-client';
es6Promise.polyfill()

import esc from 'jquery-param';

const LOCALSTORAGE_KEY = 'inconcert-api-client';

class inConcertAPIClient extends RESTClient {
  constructor(baseUrl) {
    super(baseUrl, {
      AUTH_PATH: '/inconcert/auth/password/',
      LOGOUT_PATH: '/inconcert/auth/logout/'
    })

    this.forgotPassword = this._resource({
      route: 'auth/forgot-password'
    })

    this.resetPassword = this._resource({
      route: 'auth/reset-password'
    })
    
    this.preRegistration = this._resource({
      route: 'inconcert/pre-registration'
    })

    this.register = this._resource({
      route: 'inconcert/registration/password'
    })
    
    this.me = this._resource({
      route: 'inconcert/me'
    })
    
    this.subscriptions = this._resource({
      route: 'inconcert/subscriptions'
    })

    this.artists = this._resource({
      route: 'inconcert/artists'
    })

    this.venues = this._resource({
      route: 'inconcert/venues'
    })

    this.events = this._resource({
      route: 'inconcert/events'
    })

    this.artistUpdates = this._resource({
      route: 'inconcert/artist-updates'
    })

    this.venueUpdates = this._resource({
      route: 'inconcert/venue-updates'
    })

    this.users = this._resource({
      route: 'users'
    })

    this.profiles = this._resource({
      route: 'profiles',
      details: [
        { routeName: 'confirm', method: 'get' }
      ]
    })

    this.cities = this._resource({
      route: 'cities'
    })

    this.provinces = this._resource({
      route: 'provinces'
    })

    this.countries = this._resource({
      route: 'countries'
    })

    this.genres = this._resource({
      route: 'genres',
      details: [
        { routeName: 'root', method: 'get' }
      ]
    })
  }
}

export default inConcertAPIClient;

export { LOCALSTORAGE_KEY }