'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LOCALSTORAGE_KEY = undefined;

var _es6Promise = require('es6-promise');

var _es6Promise2 = _interopRequireDefault(_es6Promise);

var _restClient = require('rest-client');

var _restClient2 = _interopRequireDefault(_restClient);

var _jqueryParam = require('jquery-param');

var _jqueryParam2 = _interopRequireDefault(_jqueryParam);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

_es6Promise2.default.polyfill();

var LOCALSTORAGE_KEY = 'inconcert-api-client';

var inConcertAPIClient = function (_RESTClient) {
  _inherits(inConcertAPIClient, _RESTClient);

  function inConcertAPIClient(baseUrl) {
    _classCallCheck(this, inConcertAPIClient);

    var _this = _possibleConstructorReturn(this, (inConcertAPIClient.__proto__ || Object.getPrototypeOf(inConcertAPIClient)).call(this, baseUrl, {
      AUTH_PATH: '/inconcert/auth/password/',
      LOGOUT_PATH: '/inconcert/auth/logout/'
    }));

    _this.forgotPassword = _this._resource({
      route: 'auth/forgot-password'
    });

    _this.resetPassword = _this._resource({
      route: 'auth/reset-password'
    });

    _this.preRegistration = _this._resource({
      route: 'inconcert/pre-registration'
    });

    _this.register = _this._resource({
      route: 'inconcert/registration/password'
    });

    _this.me = _this._resource({
      route: 'inconcert/me'
    });

    _this.subscriptions = _this._resource({
      route: 'inconcert/subscriptions'
    });

    _this.artists = _this._resource({
      route: 'inconcert/artists'
    });

    _this.venues = _this._resource({
      route: 'inconcert/venues'
    });

    _this.events = _this._resource({
      route: 'inconcert/events'
    });

    _this.artistUpdates = _this._resource({
      route: 'inconcert/artist-updates'
    });

    _this.venueUpdates = _this._resource({
      route: 'inconcert/venue-updates'
    });

    _this.users = _this._resource({
      route: 'users'
    });

    _this.profiles = _this._resource({
      route: 'profiles',
      details: [{ routeName: 'confirm', method: 'get' }]
    });

    _this.cities = _this._resource({
      route: 'cities'
    });

    _this.provinces = _this._resource({
      route: 'provinces'
    });

    _this.countries = _this._resource({
      route: 'countries'
    });

    _this.genres = _this._resource({
      route: 'genres',
      details: [{ routeName: 'root', method: 'get' }]
    });

    return _this;
  }

  return inConcertAPIClient;
}(_restClient2.default);

exports.default = inConcertAPIClient;
exports.LOCALSTORAGE_KEY = LOCALSTORAGE_KEY;
